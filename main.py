import time
import pygame
from pygame.constants import *

import globals
from Digit import Digit


def main():
    pygame.init()
    fps_clock = pygame.time.Clock()

    number_length = 5
    font = globals.font_2
    position_x = 50
    position_y = 50
    digits = []
    value = [1, 7, 8, 9, 5]
    speed = [10, 60, 60, 60, 60]
    change = [False] * number_length
    previous = [0] * number_length

    surface = pygame.display.set_mode((2*position_x + number_length*font.width, 150))

    for f in range(number_length):
        digits.append(Digit(font, position_x + (number_length-f-1)*font.width, position_y, value[f], speed[f]))

    now = time.time()

    while True:
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                exit()

        surface.fill((255, 255, 255))

        # Calculate every-tick data:
        for f in range(number_length):
            digits[f].update()
            digit_data = digits[f].get_data()
            if digit_data.value == 9 and previous[f] != 9:
                change[f] = True
            previous[f] = digit_data.value

        # Calculate time-depended data:
        if time.time() > now + 0.25:
            now += 0.25
            digits[0].start_animate()
            for f in range(0, number_length):
                if change[f] and f < number_length-1 and digits[f].get_animate():
                    digits[f+1].start_animate()
                    change[f] = False

        # Display all digits:
        for f in range(number_length):
            digit_data = digits[f].get_data()
            surface.blit(digit_data.picture, (digit_data.pos_x, digit_data.pos_y), (0, digit_data.cut, font.width, digit_data.height))

        pygame.display.update()
        fps_clock.tick(globals.FPS)


if __name__ == '__main__':
    main()
