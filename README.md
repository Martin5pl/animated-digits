### Animated digits

Initial version of an animated counter

**Required components:**  
 * Python 3.7  
 * Pygame

**To install plugins:**  
```pip3 install -r requirements.txt```


**To start:**  
```python main.py```
 
