# Global variables and constants

import pygame
from Font import Font

FPS = 60

FONT_1 = pygame.image.load('pictures/font1.png')
FONT_2 = pygame.image.load('pictures/font2.png')
FONT_3 = pygame.image.load('pictures/font3.png')

font_1 = Font(FONT_1, first=16, height=50, width=46, distance=60, last=616)
font_2 = Font(FONT_2, first=14, height=44, width=40, distance=50, last=514)
font_3 = Font(FONT_3, first=7, height=28, width=24, distance=30, last=307)
