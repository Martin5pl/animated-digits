import pygame

import Font


class Digit:
    _position_x: int
    _position_y: int
    _starting_point: int
    _next_digit_position: int
    _animate = False
    _value = 0

    _PICTURE: pygame.Surface
    _DISTANCE: int
    _FIRST_POSITION: int
    _DIGIT_HEIGHT: int
    _LAST_POSITION: int
    _SPEED: float

    def __init__(self, font: Font, pos_x, pos_y, value=0, frames=15):
        self._position_x = pos_x
        self._position_y = pos_y

        self._PICTURE = font.picture
        self._DISTANCE = font.distance
        self._FIRST_POSITION = font.first
        self._DIGIT_HEIGHT = font.height
        self._DIGIT_WIDTH = font.width
        self._LAST_POSITION = font.last

        self._value = value
        self._SPEED = self._DISTANCE / frames
        self._starting_point = self._FIRST_POSITION + value * self._DISTANCE
        self._next_digit_position = self._starting_point + self._DISTANCE

    def get_data(self):
        result = {'picture': self._PICTURE,
                  'pos_x': self._position_x,
                  'pos_y': self._position_y,
                  'cut': self._starting_point,
                  'height': self._DIGIT_HEIGHT,
                  'width': self._DIGIT_WIDTH,
                  'value': self._value}
        return AttributeDict(result)

    def start_animate(self):
        self._animate = True

    def get_animate(self):
        return self._animate

    def update(self):
        if self._animate:
            self._starting_point += self._SPEED
            if self._starting_point >= self._next_digit_position:
                self._starting_point = self._next_digit_position
                self._next_digit_position += self._DISTANCE
                self._value = (self._value + 1) % 10
                self._animate = False
                if self._starting_point >= self._LAST_POSITION:
                    self._starting_point = self._FIRST_POSITION
                    self._next_digit_position = self._starting_point + self._DISTANCE


class AttributeDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
