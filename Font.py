from dataclasses import dataclass
import pygame


@dataclass
class Font:
    picture: pygame.Surface
    first: int
    height: int
    width: int
    distance: int
    last: int
